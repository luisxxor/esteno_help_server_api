## Antes de instalar

 - Debes tener instalado Node 12.x o superior.
 - Debes tener permisos de
   administrador y usar como terminal la Powershell.
   

> Oprime Tecla Windows + R, escribe "powershell" y clickea ok para abrir la Powershell.

## Instrucciones para instalar

Descarga el proyecto y colocalo en una carpeta, por ejemplo:

    C:\esteno_help_server_api

Ahora abre el terminal en el directorio del proyecto, usando por ejemplo:

    cd C:\esteno_help_server_api
Una vez alli, hay que instalar las dependencias del proyecto, eso se hace utilizando el comando:

    npm install
Al finalizar la instalacion de las dependencias, debes introducir las credenciales de la base de datos en el archivo `db_credentials.json`.

Luego de eso, hay que instalar el servicio en windows, eso se hace con el script que trae el projecto, solo hay que ejecutar como administrador el archivo `installService.bat`.

Luego hay que revisar si el servicio se instalo correctamente, abriendo la lista de servicios de Windows.

> Oprime Tecla Windows + R, escribe "services.msc" y clickea ok para abrir la lista de servicios.

Si el servicio de Esteno Help no fue instalado correctamente, entonces la alternativa es, con el terminal abierto en la carpeta del proyecto, ejecutar el comando `node installService.js`