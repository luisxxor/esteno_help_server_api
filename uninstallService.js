var Service = require('node-windows').Service;

var svc = new Service({
  name: 'Esteno Help Server Api',
  description: 'REST API de la app de moderacion de Esteno Help Server.',
  script: require('path').join(__dirname,'index.js')
});

svc.on('uninstall',function(){
  console.log('Uninstall complete.');
  console.log('The service exists: ',svc.exists);
});

svc.uninstall();