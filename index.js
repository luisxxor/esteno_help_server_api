'use strict'

const express = require('express')
const app = express()
const http = require('http').createServer(app)
const cors = require('cors')
const io = require('socket.io').listen(http)
const fs = require('fs')
const mysql = require('mysql')
const multer = require('multer')
const expressjwt = require('express-jwt')
const jwt = require('jsonwebtoken')
const eol = require('eol')

let isProd = process.env.NODE_ENV === 'production'
if(isProd) {
  var EventLogger = require('node-windows').EventLogger;
  var log = new EventLogger('Esteno Help Server Api');
}

function printOut(arg) {
  return isProd ? log.info(arg) : console.log(arg)
}

const secret  = { secret: process.env.SECRET || 'example' }

// Credentials

let db_credentials = JSON.parse(fs.readFileSync('db_credentials.json'));

// Pool

let pool = mysql.createPool(db_credentials);

// Storage

var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'sounds')
  },
  filename: function (req, file, cb) {
    cb(null, file.originalname )
  }
})
 
var upload = multer({ storage: storage })

app.use(express.static("dist"));

app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use(cors());

let session_ids = []

app.post('/logout', function (req, res) {
  let id = req.body.id
  let index = session_ids.indexOf(id)

  if(index != -1) {
    session_ids.splice(index,1)
  }

  res.send(JSON.stringify({success: true}))
})

app.post('/login', function (req, res) {
  pool.getConnection(function(err, con) {
    if (err) throw err;
    con.query("SELECT password_server FROM config",
    function (err, result, fields) {
      if (err) throw err;

      if(req.body.password === result[0].password_server) {

        let session_id = 1
        while(session_ids.indexOf(session_id) != -1) {
          session_id++
        }

        session_ids.push(session_id)
        printOut('Sesiones actuales: ' + JSON.stringify(session_ids))

        let token = jwt.sign({id: session_id}, secret.secret, {
          expiresIn : 60 * 24
        });
        res.json({
          success: true,
          id: session_id,
          token
        })
      } else {
        res.send(JSON.stringify({success: false}))
      }
    });
    con.release();
  })
  
})

app.post('/uploadFile', upload.single('file'), function (req, res) {
 
  pool.getConnection(function(err, con) {
    if (err) throw err;
    con.query(`INSERT INTO alarms (filename) VALUES ('${req.file.originalname}')`,
    function (err, result) {
      if (err) throw err;
      res.json({success: (req.file && result.affectedRows === 1)})
    });
    con.release();
  })
})

app.get('/getFile', function(req,res) {

  let filename = '';

  pool.getConnection(function(err, con) {
    if (err)  {
      con.release();
      throw err;
    } else {    
      con.query("SELECT filename FROM alarms WHERE id = ?",[req.query.id],
      function (err, result, fields) {
        if (err) {
          con.release();
          throw err;
        }

        if(result.length === 0) {
          con.release();
          return res.status(404);
        } else {
          filename = result[0].filename;
          if (fs.existsSync(__dirname+'/sounds/'+filename)) {
            res.sendFile(__dirname+'/sounds/'+filename);
            con.release();
          } else {
            con.query("DELETE FROM alarms WHERE id = ?",[req.query.id],
              function (err, result) {
                if (err) throw err;
                con.release();
              })
          }
        }
      });
    }
  })
})

app.get('/alarms', function(req, res) {
  pool.getConnection(function(err, con) {
    if (err) throw err;
    con.query("SELECT id, filename FROM alarms",
    function (err, result, fields) {
      if (err) throw err;
      res.json(result);
    });
    con.release();
  })
})

app.post('/deleteAlarm', expressjwt(secret), function(req, res) {
  const id = req.body.id;
  const filename = req.body.filename;

  pool.getConnection(function(err, con) {
    if (err) throw err;
    con.query("DELETE FROM alarms WHERE id = ?",[id],
      function (err, result) {
        if (err) throw err;
          fs.unlinkSync(__dirname + '/sounds/' + filename)
          res.json(result)
      });
    con.release();
  })
})

app.get('/config', expressjwt(secret), function(req, res) {
  pool.getConnection(function(err, con) {
    if (err) throw err;
    con.query("SELECT password_client, password_server FROM config",
    function (err, result, fields) {
      if (err) throw err;
      res.json(result[0]);
    });
    con.release();
  })
})

app.post('/config', expressjwt(secret), function(req, res) {

  pool.getConnection(function(err, con) {
    if (err) throw err;
    let data = JSON.parse(req.body.config);
    con.query("UPDATE config SET ?", data ,
    function (err, result) {
      if (err) throw err;
      res.json(result);
    });
    con.release();
  })
})

app.post('/checkFileNameAvailable', expressjwt(secret), function(req, res) {
  pool.getConnection(function(err, con) {
    if (err) throw err;
    con.query("SELECT COUNT(id) as inuse FROM alarms WHERE filename = ?",[req.body.filename],
    function (err, result, fields) {
      if (err) throw err;
      res.json(result[0].inuse);
    });
    con.release();
  })
})

app.get('/getLog', expressjwt(secret), function (req, res) {
  pool.getConnection(function(err, con) {
    if (err) throw err;
    con.query("SELECT * FROM log ",
    function (err, result, fields) {
      if (err) throw err;
      res.json(result);
    });
    con.release();
  })
})

app.get('/getLogTxt'/*, expressjwt(secret)*/, function (req, res) {
  pool.getConnection(function (err, con) {
    if (err) throw err;
    con.query("SELECT * FROM log ",
    function (err, result, fields) {
      if (err) throw err;
      var Table = require('easy-table')
      var t = new Table

      result.forEach(function(call) {
        t.cell('Id', call.id)
        t.cell('Nombre PC', call.machine_name)
        t.cell('Tipo', call.role)
        t.cell('Fecha', call.date)
        t.cell('Hora', call.time)
        t.newRow()
      })

      res.set("Content-Type", "application/octet-stream");
      res.set("Content-Disposition", "attachment;filename=log.txt");
      printOut(eol.auto(t.toString()))
      res.send(eol.auto(t.toString()))

    });
    con.release();
  })
})

http.listen(8080, function(){
  isProd ? printOut('Escuchando en el puerto 8080.') : console.log()
});

let call_list = [];

var mods = io.of('/mods')
	.on('connection', function(socket) {
    printOut('Un moderador se ha conectado.')
    socket.emit('updateCallList', JSON.stringify(call_list));
		socket.on('disconnect', function() {
      printOut('Un moderador se ha desconectado.')
		})
	})

var clients = io.of('/client')
	.on('connection', function(socket) {
    printOut('Un usuario se ha conectado.')
		socket.on('toggleCall',function(data) {
			const { calling, machineName, role } = JSON.parse(data)

			const message = calling ? `It's calling` : `It's not calling`

			if (calling) {
        const zerofill = number => '0'+number;
        const smartZerofill = number => number < 10 ? zerofill(number) : number;
        printOut(`El ${role} ${machineName} está llamando.`);

        let d = new Date();
        let date = `${d.getFullYear()}-${ smartZerofill(d.getMonth()) }-${ smartZerofill(d.getDate()) }`
        let time = `${ smartZerofill(d.getHours()) }:${ smartZerofill(d.getMinutes()) }:${ smartZerofill(d.getSeconds())}`
				call_list.push({role: role, machineName: machineName, socket_id: socket.id, date: date, time: time})
        pool.getConnection(function(err, con) {
          if (err) throw err;
          con.query(`INSERT INTO log (machine_name, role, date, time) VALUES ("${machineName}","${role}",CURRENT_DATE(),CURRENT_TIME()) `,
          function (err, result) {
            if (err) throw err;
          });
          con.release();
        })
			} else {
				call_list = call_list.filter(x => x.machineName != machineName)
        printOut(`La llamada del ${role} ${machineName} se ha detenido.`);
			}

      io.of('/mods').emit('updateCallList',JSON.stringify(call_list))
      socket.emit('callStatusChanged', JSON.stringify(calling))
      printOut(`Call status changed to ${calling ? 'calling' : 'not calling'}`)

		});

		socket.on('disconnect',function() {
      call_list = call_list.filter(x => x.socket_id != socket.id)
      io.of('/mods').emit('updateCallList',JSON.stringify(call_list))
      printOut('Usuario desconectado.')
		});
	})

