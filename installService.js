var Path = require('path');
var Service = require('node-windows').Service;
 
var svc = new Service({
  name:'Esteno Help Server Api',
  description: 'REST API de la app de moderacion de Esteno Help Server.',
  script: require('path').join(__dirname,'index.js')
});
 
svc.on('install',function(){
  svc.start();
});

svc.install();